Imports System.IO
Imports Newtonsoft.Json
Imports RiskAssessmentDemo.Entities.RiskAssessmentDemo.Entities

Namespace RiskAssessmentDemo.Business
    Public Class RiskAssessmentEvaluationLogic
        Private Const EvaluationDirectory As String = "~\data\evaluations\"

        Public Shared Sub InsertAssessment(evaluation As RiskAssessmentEvaluation)
            Dim evaluationJson As String = JsonConvert.SerializeObject(evaluation)
            
            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(EvaluationDirectory))
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath($"{EvaluationDirectory}{evaluation.Date.ToString("yyyy-dd-M-HH-mm-ss")}.json"), evaluationJson)
        End Sub

        Public Shared Function GetAssessments() As List(Of RiskAssessmentEvaluation)
            If Directory.Exists(System.Web.HttpContext.Current.Server.MapPath($"{EvaluationDirectory}")) Then
                Return Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath($"{EvaluationDirectory}")) _
                    .Select(Function(f)
                        Dim fileContent As String = File.ReadAllText(f)
                        Return JsonConvert.DeserializeObject (Of RiskAssessmentEvaluation)(fileContent)
                    End Function) _
                    .ToList()
            Else
                Return New List(Of RiskAssessmentEvaluation)()
            End If
        End Function
    End Class
End NameSpace