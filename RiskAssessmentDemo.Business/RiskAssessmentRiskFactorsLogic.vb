﻿Imports RiskAssessmentDemo.Entities.RiskAssessmentDemo.Entities

Namespace RiskAssessmentDemo.Business
    Public Class RiskAssessmentRiskFactorsLogic
        Public Shared Function GetRiskFactors() As List(Of RiskAssessmentRiskFactor)
            Return New List(Of RiskAssessmentRiskFactor) From {
                New RiskAssessmentRiskFactor With{
                    .Id  =1,
                    .Description ="Less than 50 Hours in Aircraft or Avionics Type ",
                    .Weight =1
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =2,
                    .Description ="Less than 15 hours in last 90 days ",
                    .Weight =3
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =3,
                    .Description ="Flight will occur after work  ",
                    .Weight =2
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =4,
                    .Description ="Less than 8 hours sleep prior to flight  ",
                    .Weight =2
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =5,
                    .Description ="Dual Instruction Received in last 90 days",
                    .Weight =1
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =6,
                    .Description ="WINGS Phase Completion in last 6 months ",
                    .Weight =- 3
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =7,
                    .Description ="Instrument Rating current and proficient",
                    .Weight =- 1
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =8,
                    .Description ="Previous flight was less 2 hours prior ",
                    .Weight =2
                    },
                New RiskAssessmentRiskFactor With{
                    .Id  =9,
                    .Description ="Flight is at night",
                    .Weight =2
                    }
                }
        End Function
    End Class
End Namespace

