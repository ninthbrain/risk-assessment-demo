﻿<%@ Page Title="Contact" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.vb" Inherits="RiskAssessmentDemo.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p></p>

    <address>
        481 N. Main St. Ste 148<br />
        Frankenmuth, MI 48734<br />
        (888) 364-9995<br />
    </address>

    <address>
        info@ninthbrain.com
    </address>
</asp:Content>
