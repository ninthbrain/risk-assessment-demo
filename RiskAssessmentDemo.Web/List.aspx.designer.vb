﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class List
    
    '''<summary>
    '''pnlMessageBox control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify, move the field declaration from the designer file to a code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessageBox As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''pnlStaticSection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify, move the field declaration from the designer file to a code-behind file.
    '''</remarks>
    Protected WithEvents pnlStaticSection As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''rlvAssessments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify, move the field declaration from the designer file to a code-behind file.
    '''</remarks>
    Protected WithEvents rlvAssessments As Global.System.Web.UI.WebControls.ListView
    
    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As RiskAssessmentDemo.SiteMaster
        Get
            Return CType(MyBase.Master, RiskAssessmentDemo.SiteMaster)
        End Get
    End Property
End Class
