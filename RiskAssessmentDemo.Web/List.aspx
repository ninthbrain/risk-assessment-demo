﻿<%@ Page Title="Past Assessments" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.vb" Inherits="RiskAssessmentDemo.List" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-6">
            <asp:Panel ID="pnlMessageBox" runat="server" CssClass="alert alert-success" Visible="False" style="margin-top: 10px;">
                <div>Evaluation Saved</div>
            </asp:Panel>
            <asp:Panel ID="pnlStaticSection" runat="server" CssClass="box box-success">
                <div class="box-header">
                    <h4 class="box-title">
                        <h4>Past Assessments</h4>
                    </h4>
                </div>
                <div class="box-body">
                    <asp:ListView ID="rlvAssessments" runat="server">
                        <LayoutTemplate>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Score</th>
                                </tr>
                                </thead>
                                <tr id="itemPlaceHolder" runat="server">
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="col-md-1">
                                    <asp:Label runat="server" Text='<%# Eval("Date")%>'></asp:Label>
                                </td>
                                <td class="col-md-2 text-center">
                                    <asp:Label runat="server" Text='<%# Eval("Score")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
