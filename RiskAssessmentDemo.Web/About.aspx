﻿<%@ Page Title="About" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.vb" Inherits="RiskAssessmentDemo.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>
        Simple VB.net ASP.NET Web Forms demo project that allows users to fill out a simple form and get an evaluation if they should proceed with the flight.
    </p>
</asp:Content>
