﻿Imports RiskAssessmentDemo.Business.RiskAssessmentDemo.Business
Imports RiskAssessmentDemo.Entities.RiskAssessmentDemo.Entities

Public Class _Default
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.rlvRiskFactors.DataSource = RiskAssessmentRiskFactorsLogic.GetRiskFactors()
            Me.rlvRiskFactors.DataBind()
        End If
    End Sub

    Protected Sub btnSubmit_OnClick(sender As Object, e As EventArgs)
        Dim score As Integer
        For Each dataItem As ListViewDataItem In Me.rlvRiskFactors.Items
            Dim chkSelect As CheckBox = dataItem.FindControl("chkSelect")
            Dim lblWeight As Label = dataItem.FindControl("lblWeight")
            
            If chkSelect.Checked Then
                score += lblWeight.Text
            End If
        Next
        
        Dim recordedAssessment = New RiskAssessmentEvaluation With {
                .Date = DateTime.Now,
                .Score = score
                }
        RiskAssessmentEvaluationLogic.InsertAssessment(recordedAssessment)
        
        Me.pnlMessageBox.Visible = True
    End Sub
End Class