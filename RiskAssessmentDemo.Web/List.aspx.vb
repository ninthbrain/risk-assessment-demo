﻿Imports RiskAssessmentDemo.Business.RiskAssessmentDemo.Business
Imports RiskAssessmentDemo.Entities.RiskAssessmentDemo.Entities

Public Class List
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.rlvAssessments.DataSource = RiskAssessmentEvaluationLogic.GetAssessments()
            Me.rlvAssessments.DataBind()
        End If
    End Sub
End Class