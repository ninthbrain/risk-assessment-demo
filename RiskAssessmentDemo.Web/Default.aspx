﻿<%@ Page Title="New Risk Assessment" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="RiskAssessmentDemo._Default" %>

<%@ Import Namespace="RiskAssessmentDemo.Entities.RiskAssessmentDemo.Entities" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:Panel ID="pnlMessageBox" runat="server" CssClass="alert alert-success" Visible="False" Style="margin-top: 10px;">
                <div>Evaluation Saved</div>
            </asp:Panel>
            <div class="box-header">
                <h4 class="box-title">
                    <h4>Risk Factors</h4>
                </h4>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box-body">
                <div>Check the risk factors that apply to you:</div>
                <asp:ListView ID="rlvRiskFactors" runat="server">
                    <LayoutTemplate>
                        <table class="table table-bordered table-striped">
                            <tr id="itemPlaceHolder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="risk-factor-row" data-weight="<%# CType(Container.DataItem, RiskAssessmentRiskFactor).Weight %>">
                            <td class="col-md-1">
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkSelect"></asp:CheckBox>
                                <asp:HiddenField ID="hdnId" Value='<%# Eval("Id") %>' runat="server" />
                            </td>
                            <td class="col-md-7">
                                <asp:Label runat="server"><%# Eval("Description") %></asp:Label>
                            </td>
                            <td class="col-md-2 text-center">
                                <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("Weight") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div class="box-tools pull-right">
                    <asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="btnSubmit_OnClick">Submit</asp:LinkButton>
                </div>
            </div>
        </div>
        <div id="divGauge" class="col-md-6" runat="server">
        </div>
    </div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/justgage/1.2.9/justgage.min.js"></script>
    <script>
        $(function () {
            var gauge = new JustGage({
                id: "<%= divGauge.ClientID %>", 
                value: 0,
                min: 0,
                max: 10,
                decimals: 0,
                gaugeWidthScale: 1
            });

            function UpdateGage() {
                var score = 0;
                $('.risk-factor-row').each(function () {
                    if ($(this).find("input:checked").length != 0) {
                        var weight = $(this).data("weight");
                        score += weight;
                    }
                });
                gauge.refresh(score);
            }

            $('.chkSelect').change(UpdateGage);
            UpdateGage();
        });
    </script>
</asp:Content>
