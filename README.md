# Risk Assessment Demo

## Welcome
We want to work with the best engineers. We think writing code is a better way to find great engineers rather than trivia questions during the interview. This is a simple example project that we want you to make some basic changes to.

## What does this project do?
This is an example project that lets the users evaluate the risk of a flight. Users create an assessment by checking risk factors that apply to them. Past evaluations can also be viewed.
![](screenshot.png)


## Building and running the project
### Getting the source
You can get a copy of the source:

*  by using any git client and the url https://bitbucket.org/ninthbrain/riskassessmentdemo.git
* or by downloading zip of the source at https://bitbucket.org/ninthbrain/riskassessmentdemo/downloads/ 

### Running the project
You will need Visual Studio 2019 to build and run this project. You can use the [free community edition](https://visualstudio.microsoft.com/vs/), just make sure to include the "ASP.NET and web development" features when installing it. Once you open the "RiskAssessmentDemo.sln" solution in Visual Studio you can use F5 to build and run the project.

## Requested changes
Curently the application only logs a final risk score.
We'd like you to make two changes:
### 1. Provide feedback to the user when they submit the form if their risk is within acceptable ranges. When the user submits the form show a message that says:
| Message     	| Score Range 	|
|-------------	|-------------	|
| Low Risk    	| Less than 5 	|
| Medium Risk 	| Less than 8 	|
| High Risk   	| Above 8     	|

### 2. Add a text field that users can record notes to
Provide a new text field for users to provide additional comments with their assessment. Keep in mind they may want to review these later.

## When your done
Once you complete your changes you can:

1. Zip your project to email dev@ninthbrain.com. Just make sure to delete the `packages` directory since it contains a large number of assemblies that don't need to be in the zip.
2. or Email dev@ninthbrain.com a link to a repository with your changes.